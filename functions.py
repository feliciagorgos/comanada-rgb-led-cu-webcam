import numpy as np
import cv2 as cv

Y_RANGE = 48 / 100
X_RANGE = 48 / 100
HEIGHT_RANGE = 4 / 100
WIDTH_RANGE = 4 / 100
COLOUR = (255, 0, 0)

def createAverageColourImage(rgb_values : tuple, dimensions : tuple):

    image = np.zeros((dimensions[0], dimensions[1], 3), np.uint8)

    if len(rgb_values) != 3:
        return (1, image)
    
    for i in range(3):
        if rgb_values[i] < 0 or rgb_values[i] > 255:
            return (i + 2, image)
    

    image[:, :] = rgb_values

    return (0, image)




def drawRectangle(image : np.ndarray) :

    pt1 = (int(float(image.shape[1]) * Y_RANGE) - 2, int(float(image.shape[0]) * X_RANGE) - 2)
    pt2 = (int(float(image.shape[1]) * (Y_RANGE + HEIGHT_RANGE)) + 2, int(float(image.shape[0]) * (X_RANGE + WIDTH_RANGE)) + 2)

    rect = cv.rectangle(image, pt1, pt2, COLOUR, thickness = 2)

    return rect



def createIndicators(b : int, g : int, r : int):

    if b < 0:   b = 0
    if b > 255: b = 255
    if g < 0:   g = 0
    if g > 255: g = 255
    if r < 0:   r = 0
    if r > 255: r = 255

    window = np.zeros((700, 390, 3), np.uint8)
    window[:, :] = (100, 100, 100)

    
    red_bkg = ((60, 100), (90, 610), (255, 255, 255), cv.FILLED)
    green_bkg = ((180, 100), (210, 610), (255, 255, 255), cv.FILLED)
    blue_bkg = ((300, 100), (330, 610), (255, 255, 255), cv.FILLED)        

    red_column = ((60, 100), (90, 610), (0, 0, 0), 2)
    green_column = ((180, 100), (210, 610), (0, 0, 0), 2)
    blue_column = ((300, 100), (330, 610), (0, 0, 0), 2)
    
    red_fill = ((60, 610 - 2 * r), (90, 610), (0, 0, 255), cv.FILLED)
    green_fill = ((180, 610 - 2 * g), (210, 610), (0, 255, 0), cv.FILLED)
    blue_fill = ((300, 610 - 2 * b), (330, 610), (255, 0, 0), cv.FILLED)

    red_indicator = ((30, 640), (120, 670), (255, 255, 255), cv.FILLED)
    green_indicator = ((150, 640), (240, 670), (255, 255, 255), cv.FILLED)
    blue_indicator = ((270, 640), (360, 670), (255, 255, 255), cv.FILLED)

    if r < 100:
        red_hundreds = ''
    else:
        red_hundreds = str(r // 100)
    if r < 10:
        red_tens = ''
    else:
        red_tens = str((r // 10) % 10)
    red_units = str(r % 10)
    if g < 100:
        green_hundreds = ''
    else:
        green_hundreds = str(g // 100)
    if g < 10:
        green_tens = ''
    else:
        green_tens = str((g // 10) % 10)
    green_units = str(g % 10)
    if b < 100:
        blue_hundreds = ''
    else:
        blue_hundreds = str(b // 100)
    if b < 10:
        blue_tens = ''
    else:
        blue_tens = str((b // 10) % 10)
    blue_units = str(b % 10)

    FONT = cv.FONT_HERSHEY_SIMPLEX
    FONT_SIZE = 1
    FONT_COLOUR = (0, 0, 0)
    FONT_THICKNESS = 2

    RED_INDEX = 'R'
    GREEN_INDEX = 'G'
    BLUE_INDEX = 'B'

    
    window = cv.rectangle(window, red_bkg[0], red_bkg[1], red_bkg[2], red_bkg[3])
    window = cv.rectangle(window, green_bkg[0], green_bkg[1], green_bkg[2], green_bkg[3])
    window = cv.rectangle(window, blue_bkg[0], blue_bkg[1], blue_bkg[2], blue_bkg[3])

    window = cv.rectangle(window, red_fill[0], red_fill[1], red_fill[2], red_fill[3])
    window = cv.rectangle(window, green_fill[0], green_fill[1], green_fill[2], green_fill[3])
    window = cv.rectangle(window, blue_fill[0], blue_fill[1], blue_fill[2], blue_fill[3])

    window = cv.rectangle(window, red_column[0], red_column[1], red_column[2], red_column[3])
    window = cv.rectangle(window, green_column[0], green_column[1], green_column[2], green_column[3])
    window = cv.rectangle(window, blue_column[0], blue_column[1], blue_column[2], blue_column[3])

    window = cv.rectangle(window, red_indicator[0], red_indicator[1], red_indicator[2], red_indicator[3])
    window = cv.rectangle(window, green_indicator[0], green_indicator[1], green_indicator[2], green_indicator[3])
    window = cv.rectangle(window, blue_indicator[0], blue_indicator[1], blue_indicator[2], blue_indicator[3])

    window = cv.putText(window, red_hundreds, (35, 665), FONT, FONT_SIZE, FONT_COLOUR, thickness = FONT_THICKNESS, bottomLeftOrigin = False)
    window = cv.putText(window, red_tens, (65, 665), FONT, FONT_SIZE, FONT_COLOUR, thickness = FONT_THICKNESS, bottomLeftOrigin = False)
    window = cv.putText(window, red_units, (95, 665), FONT, FONT_SIZE, FONT_COLOUR, thickness = FONT_THICKNESS, bottomLeftOrigin = False)

    window = cv.putText(window, green_hundreds, (155, 665), FONT, FONT_SIZE, FONT_COLOUR, thickness = FONT_THICKNESS, bottomLeftOrigin = False)
    window = cv.putText(window, green_tens, (185, 665), FONT, FONT_SIZE, FONT_COLOUR, thickness = FONT_THICKNESS, bottomLeftOrigin = False)
    window = cv.putText(window, green_units, (215, 665), FONT, FONT_SIZE, FONT_COLOUR, thickness = FONT_THICKNESS, bottomLeftOrigin = False)

    window = cv.putText(window, blue_hundreds, (275, 665), FONT, FONT_SIZE, FONT_COLOUR, thickness = FONT_THICKNESS, bottomLeftOrigin = False)
    window = cv.putText(window, blue_tens, (305, 665), FONT, FONT_SIZE, FONT_COLOUR, thickness = FONT_THICKNESS, bottomLeftOrigin = False)
    window = cv.putText(window, blue_units, (335, 665), FONT, FONT_SIZE, FONT_COLOUR, thickness = FONT_THICKNESS, bottomLeftOrigin = False)

    window = cv.putText(window, RED_INDEX, (43, 80), FONT, 3, (0, 0, 255), 4)
    window = cv.putText(window, GREEN_INDEX, (163, 80), FONT, 3, (0, 255, 0), 4)
    window = cv.putText(window, BLUE_INDEX, (283, 80), FONT, 3, (255, 0, 0), 4)

    return window



def getAverageColour(image : np.ndarray, range : tuple):
    # image = frame
    # range = ((coordinates of top left corner), (dimensions of section))

    ((Y_RANGE, X_RANGE), (HEIGHT, WIDTH)) = range
    (ROW_NR, COL_NR, CHN_NR) = image.shape

    if Y_RANGE + HEIGHT > ROW_NR or X_RANGE + WIDTH > COL_NR:
        return (1, (0, 0, 0))
    if X_RANGE < 0 or Y_RANGE < 0:
        return (2, (0, 0, 0))
    if HEIGHT < 1 or WIDTH < 1:
        return (3, (0, 0, 0))
    
    section = image[Y_RANGE : Y_RANGE + HEIGHT,  X_RANGE : X_RANGE + WIDTH]

    avg_b = 0
    avg_g = 0
    avg_r = 0
    count = 0

    for i in section:
        for j in i:
            #b, g, r = cv.split(j)
            avg_b += j[0]
            avg_g += j[1]
            avg_r += j[2]
            count += 1

    avg_b /= count
    avg_g /= count
    avg_r /= count

    return (0, (int(avg_b), int(avg_g), int(avg_r)))
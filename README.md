# Comanda RGB LED cu WebCam


## Descrierea proiectului

În cadrul proiectului, ne-am propus să controlăm culoarea unui beculeț în conformitate cu o culoare pe care i-o arătăm la camera web a laptopului. Altfel spus, atunci când la cameră arătăm o anumită culoare, beculețul să se aprindă de acea culoare. 

Pentru a face asta am folosit Python ca să accesăm camera web a device-ului nostru și să facem media câtorva pixeli din mijloc, ceea ce va fi și culoarea propriu-zisă folosită în continuare. Mediul de programare Arduino prelua aceste date prin protocolul Firmata ca să le transmită spre circuitul cu un LED de tip RGB.

Înafară de partea software, pentru o reprezentare cât mai sugestivă, am plasat circuitul cu tot cu plăcuță într-o cutie pe pereții căreia am lipit hârtie argintie (ușor reflectorizantă) rămasă de la un cadou de Crăciun :)

Peste beculețele roșii ale plăcuței am lipit bandă izolatoare neagră ca nu să interfereze cu culoarea pe care vrem noi să o reprezentăm. Astfel am încercat să creăm un mediu care ar amplifica pe cât de mult posibil culoarea beculețului întrucât lumina acestuia e destul de slabă mai ales într-un mediu luminos.
<br>

## Implementarea Proiectului
### **Elemente de hardware**

![Circuitul](circuit.jpg)

#### **Tehnologii folosite**

Pentru realizarea circuitului electric din cadrul proiectului au fost necesare următoarele componente:
  - Plăcuță Arduino 
  - Breadboard 30 x 10
  - un LED RGB
  - 3 rezistoare de 100 ohmi
  - 5 abluri

#### **Schema electrică**
<br>

![Schema electrică](schema_electrica.jpg)

După cum se observă în imaginea de mai sus, circuitul electric a fost realizat conectând cei trei anozi ai LED-ului RGB la pinurile 9, 10 și 11 din secțiunea PWM a plăcuței. De asemenea, este indispensabilă conexiunea dintre catodul LED-ului și pinul GND.

#### Mecanismul PWM (Pulse Width Modulation)
Modelele Arduino sunt fabricate fără posibilitatea de a reda output "real" analogic pe niciunul din pinuri. Este de la sine înțeles că necesitățile utilizatorilor nu pot fi satisfăcute pe deplin fără depășirea acestei constrângeri.

Prin intermediul unor ieșiri original digitale, dezvoltatorii Arduino au reușit să "simuleze" valori analogice folosind mecanismul Pulse Width Modulation. Acesta presupune o succesiune de pulsuri de frecvență deosebit de mare și de durată variabilă. Nivelul analogic al ieșirii este reprezentat de raportul dintre durata pe care tensiunea are valori ridicate și durata totală a ciclului. De exemplu, dacă tensiunea are valori ridicate timp de 3 microsecunde, apoi scăzute timp de una singură, valoarea ieșirii este de 75%.

<br>

### **Elemente de software**

#### **Tehnologii folosite**

Pentru implementarea aplicației software am ales să folosim limbajul Python, care comunică prin intermediul protocolului Firmata cu plăcuța Arduino. De asemenea, rularea programului este facilitată de un fișier batch pentru Windows.

<br>

#### **Descrierea modulelor Python**

#### Funcțiile utilizate: [functions.py](https://gitlab.com/feliciagorgos/comanada-rgb-led-cu-webcam/-/blob/main/functions.py)

   1. `getAverageColour(image : np.ndarray, range : tuple) -> tuple[int, tuple[int, int, int]]`
      - `image` - imaginea inregistrată de camera video, sub formă de martice tridimensională cu 3 canale (R, G, B);
      - `range` - tuplu format din două alte tupluri. Primul conține coordonatele colțului din stânga sus al ariei pe care se va determina culoarea medie, al doilea conține dimensiunile (înălțime, lățime) acestei arii;
      - Returnează un tuplu ce conține două elemente:
        - un cod care semnalează succesul sau eroarea în urma executării funcției;
        - un tuplu care, in caz de succes, conține valorile RGB medii de pe aria luată în considerare;
      - Se folosește de media aritmetică.
   2. `createAverageColourImage(rgb_values : tuple, dimensions : tuple) -> tuple[int, np.ndarray]`
      - `rgb_values` - valorile RGB returnate de funcția `getAverageColour`;
      - `dimensions` - dimensiunile (înălțime, lățime) dorite pentru imaginea ce va conține culoarea medie;
      - Returnează un tuplu ce conține două elemente:
        - un cod care semnalează succesul sau eroarea în urma executării funcției;
        - o matrice tridimensională cu 3 canale (R, G, B) ce reprezintă imaginea colorată în întregime în nuanța medie;
      - Creează o imagine de dimensiunile dorite, apoi o umple cu culoarea relevantă.
   3. `createIndicators(b : int, g : int, r : int) -> np.ndarray`
      - `r  g  b ` - valorile RGB ce trebuie afișate pe axele indicatorilor;
      - Returnează o matrice tridimensională cu 3 canale (R, G, B) ce reprezintă secțiunea grafică a indicatoarelor de intensitate pentru fiecare culoare elementară.
      - În funcție de inputul furnizat, această funcție desenează frame-urile pe rând și creează impresia de continuitate.
   4. `drawRectangle(image : np.ndarray) -> np.ndarray`
      - `image` - captura camerei video, pe care trebuie fixate granițele ariei de interes (cea pe care se va calcula culoarea medie);
      - Returnează imaginea alterată prin desenarea dreptunghiului.

#### Scriptul principal [project.py](https://gitlab.com/feliciagorgos/comanada-rgb-led-cu-webcam/-/blob/main/project.py)

Scriptul este împărțit în 3 secțiuni:
   1. Setup-ul
      - Primul lucru care se realizează aici este conectarea la plăcuța Arduino, prin intermediul portului 'COM8'.
      - Se definesc 7 constante:
        - `RED_PIN`, `GREEN_PIN`, `BLUE_PIN` - pentru a indica pinii corespunzători fiecărei culori elementare pe placă, care ulterior se și "alocă" folosind funcția `get_pin`;
        - `RED_COEFFICIENT`, `GREEN_COEFFICIENT`, `BLUE_COEFFICIENT` - le vom demonstra utilitatea mai târziu;
        - `STABILITY_RATE` - indică numărul de valori RGB care vor fi luate în calcul pentru stabilizarea culorilor. Dacă la fiecare frame am fi trimis valori radical diferite către LED, atunci culoarea medie ar fi fost dificil de aproximat. Folosind acest mecanism de stabilizare, LED-ul primește ca input media ultimelor `STABILITY_RATE` eșantioane;
      - Se obține primul cadru de la camera video.
   2. Loop-ul
      - La fiecare iterație, primul lucru realizat este aflarea nuanței medii prin intermediul funcției `getAverageColour`. Ulterior, valorile aferente ei sunt adăugate în lista de eșantioane în detrimentul celor mai vechi valori din listă.
      - Dacă medierea s-a încheiat cu succes, atunci este apelată funcția `createAverageColourImage`. 
      - Dacă și acest proces s-a desfășurat fără eroare, funcția `createIndicators` primește media ultimelor `STABILITY_RATE` eșantioane și creează secțiunea indicatorilor. 
      - Mai departe, aceeași medie de eșantioane este "scrisă" la pinii aferenți celor trei culori elementare, folosind funcția `write`. Aici intră în rol coeficienții declarați în secțiunea Setup. Utilitatea lor va fi detaliată în capitolul "Obstacole întâmpinate în implementare".
      - În final, cele 3 secțiuni vizuale (frame-ul de la cameră, culoarea medie și indicatorii) sunt concatenate și afișate pe ecran, apoi un nou cadru este preluat de la camera video.
   3. Cleanup section
      - Se face "curățenie" în urma rulării programului.
      - Mai exact, pinurile Arduino sunt setate înapoi pe 0, iar fereastra interfeței vizuale este închisă.

<br>

#### **Descrierea protocolului Firmata**

`Firmata` este un protocol standard care stabilește o conexiune între scriptul Python și plăcuța Arduino. De interes pentru noi este sketch-ul built-in StandardFirmata, care trebuie încărcat pe plăcuță înainte de orice interacțiune cu Python. In acest sens, poate fi accesat prin `File > Examples > Firmata > StandardFirmata`.

Pentru utilizarea protocolului în cadrul unui script .py, este necesară instalarea bibliotecii `pyfirmata`, folosind comanda `pip install pyfirmata`, și importarea modulului omonim în fișierul .py.

<br>

#### **Modul de rulare a aplicației**

Pentru a spori accesibilitatea aplicației și de către persoane cu un nivel de pregătire redus în domeniu, dar și pentru a înlesni procedura de rulare, am pus la punct un fișier .bat destinat executării în Windows Command Prompt. Mai întâi, folosind comanda `cd`, user-ul ajunge în folderul dorit, urmând ca scriptul să fie rulat folosind comanda `python3`.

<br>

#### **Interfața video**

<br>

![Interfața Grafică](interfata_grafica.jpg)

Se observă că interfața grafică este formată din 3 secțiuni:
  - Cadrul video - redă în timp real frame-urile capturate de cameră;
  - Indicatori RGB - indică nivelurile celor 3 culori elementare;
  - Culoarea medie - prezintă media culorilor din dreptunghiul albastru;

<br>

## Obstacole întâmpinate la implementare

#### Calibrarea canalelor R, G și B 

Încă de la primele încercări se observa că orice culoare am fi transmis spre LED, se păstra o nuanță puternică de albastru amestecat cu verde, problemă pe care nu am prevăzut-o. În primul rând ne-a fost extrem de greu să obținem un alb cât de cât curat din această cauză, respectiv experimentând de mai multe ori cu multipli aplicați asupra canalelor B și G, am obținut cam cea mai acceptabilă versiune luând seama și de următoarea problemă: 

----
#### Găsirea elementelor de circuit potrivite

Am observat că nu există o formulă universală pentru a calibra orice LED RGB astfel încât să se poată obține de exemplu alb. În primul rând am folosit rezistențe care ar asigura faptul că nici un canal din cele 3 nu o să se ardă de la curentul din circuit. Totuși această soluție s-a dovedit a nu fi suficientă și a trebuit să reglăm și valorile efective pe care le transmitem pe fiecare canal.

----
#### Influența camerei web 

Din cauză că WebCam-ul de la laptop nu este tocmai calitativ, ne-a fost greu să testăm anumite culori. De exemplu, dacă arătam la cameră un roșu intens, camera ajusta lumina și în final "citea" roz. Beculețul prelua teoretic culoarea corectă a pixelilor și aria respectivă, numai că nu se aprindea fix de culoarea care am vrut noi să fie transmisă.

<br>

## Demonstrația funcționării
### [Video Simulare](https://www.youtube.com/watch?v=tk7uMvPi474)

<br>

## Surse
   - https://www.led-professional.com/resources-1/articles/avoiding-brightness-and-color-mismatch-with-proper-rgb-gamut-calibration
   - https://realpython.com/arduino-python/#using-analog-outputs
   - https://howtomechatronics.com/tutorials/arduino/how-to-use-a-rgb-led-with-arduino/

<br>

## Referințe [proiecte asemănătoare]

#### [Chameleon Scarf](https://learn.adafruit.com/chameleon-scarf/overview)

Spre deosebire de ideea pe care am ales să o implementăm noi, aici se folosește senzor de culoare în loc de cameră web. În plus, culoarea se citește apăsând un buton, după care se va colora fularul în acea culoare, pe când noi lăsăm becul să se schimbe în timp real atâta timp cât este camera pornită. Oricum este o prezentare mai degrabă a unei idei comerciale, respectiv scopul este să se promoveze elementele de circuit folosite.

----
#### [Colour Detection using WebCam](https://www.robotique.tech/robotics/real-time-color-detection-using-webcam-and-arduino/)

Principala diferență între această implementare și ideea noastră este că aici doar se identifică una din cele 3 culori primare ale luminii și se aprinde un bec de culoarea respectivă la detectare. În plus, implementarea nu este de timp real "continuu", circuitul se activează ca să aprindă un bec doar atunci când i se arată culoarea pe care o detectează. La nivel de cod, aici se folosește librăria Serial, pe când noi am folosit protocolul Firmata.

----
#### [DIY Mood Lamp](https://www.youtube.com/watch?v=bpyNu5Ayx9o)

Această implementare este cu mult diferită față de ce ne-am gândit să facem noi, dar nu am mai găsit alte exemple elocvente în care să se folosească controlul unui LED de tip RGB cu o culoare dictată de o sursă "necontrolabilă" în mod direct. Așadar, principala diferență aici este faptul că s-au setat din start anumite culori ce vor fi folosite pe parcurs. De asemenea, se folosește integral un circuit independent, pe când implementarea noastră face legătura între mediile Python și Arduino.

<br>





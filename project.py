import cv2
from functions import *

import pyfirmata

board = pyfirmata.Arduino('COM9')

# it = pyfirmata.util.Iterator(board)
# it.start()

RED_PIN = 9
GREEN_PIN = 10
BLUE_PIN = 11

RED_COEFFICIENT = 1
BLUE_COEFFICIENT = 0.25
GREEN_COEFFICIENT = 0.35

blue_pin = board.get_pin(f'd:{BLUE_PIN}:p')
green_pin = board.get_pin(f'd:{GREEN_PIN}:p')
red_pin = board.get_pin(f'd:{RED_PIN}:p')

cv2.namedWindow("Video")
vc = cv2.VideoCapture(0)

STABILITY_RATE = 50

if vc.isOpened(): # try to get the first frame
    rval, frame = vc.read()
else:
    rval = False

red = []
green = []
blue = []
sum_red = 0
sum_green = 0
sum_blue = 0

while rval:

    frame = drawRectangle(frame)

    code, rgb_values = getAverageColour(frame, ((int(frame.shape[0] * Y_RANGE), int(frame.shape[1] * X_RANGE)), (int(frame.shape[0] * HEIGHT_RANGE), int(frame.shape[1] * WIDTH_RANGE))))
    red.append(rgb_values[2])
    green.append(rgb_values[1])
    blue.append(rgb_values[0])
    red = red[-STABILITY_RATE : ]
    green = green[-STABILITY_RATE : ]
    blue = blue[-STABILITY_RATE : ]
    sum_red = 0
    sum_green = 0
    sum_blue = 0
    if not code:
        code_show, colour = createAverageColourImage(rgb_values, (700 - frame.shape[0], frame.shape[1]))
        if not code_show:
            for i in range(len(red)):
                sum_red += red[i]
                sum_green += green[i]
                sum_blue += blue[i]
            indicators = createIndicators(int(sum_blue / len(blue)), int(sum_green / len(green)), int(sum_red / len(red)))
            blue_pin.write(int(BLUE_COEFFICIENT * sum_blue / len(blue)) / 255)
            green_pin.write(int(GREEN_COEFFICIENT * sum_green / len(green)) / 255)
            red_pin.write(int(RED_COEFFICIENT * sum_red / len(red)) / 255)
        else:
            colour = np.zeros((700 - frame.shape[0], frame.shape[1], 3), np.uint8)

    united = np.vstack((frame, colour))
    united = np.hstack((united, indicators))
    cv2.imshow('Video', united)

    rval, frame = vc.read()
    key = cv2.waitKey(20)
    if key == 27: # exit on ESC
        break

blue_pin.write(0)
green_pin.write(0)
red_pin.write(0)

vc.release()
cv2.destroyWindow("Video")

